# A docker image to run data science notebooks

Based on [datascience-notebook](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-datascience-notebook)

## Supported Kernels:
- Python
- R
- Julia
- Javascript
- Typescript
- Bash

## Python Packages:
pandas, numexpr, matplotlib, scipy, seaborn, scikit-learn, scikit-image, sympy, cython, patsy, statsmodel, cloudpickle, dill, numba, bokeh, sqlalchemy, hdf5, vincent, beautifulsoup, protobuf, xlrd, ipywidgets, Facets, plotly, plotly-express, cufflinks, pymongo, python-dotenv, jupyter-contrib-nbextensions, jupytext, rise, voila, jupyterlab-dash, papermill, pylantern, tqdm, sidecar, qgrid, pandas-profiling and pytest

## R Packages:
dplyr, ggplot2, tidyr, readr, purrr, tibble, stringr, lubridate, broom, plyr, devtools, shiny, rmarkdown, forecast, rsqlite, reshape2, nycflights13, caret, rcurl randomforest, circlize, and plotly

## Julia Packages:
HDF5, Gadfly, and RDatasets

## NPM Packages:
dstools, rxjs, lodash, circos, and plotly-notebook-js

## Jupyter Extensions:
jupyterlab-manager, jupytext, plotlywidget, plotly-extension, jupyterlab-chart-editor, jupyterlab_voyager, jupyterlab-dash, lantern, qgrid, perspective-jupyterlab, ipysheet, lineup_widget, jupyterlab-sidecar, statusbar, toc, widgetsnbextension, hinterland, hide_input_all, varInspector, codefolding, scratchpad, collapsible_headings, execute_time, toc2, notify, voila, ipytest and jupyterlab-git

## Others:
Jupyter Notebook server, Miniconda, Pandoc, TeX Live, git, emacs, jed, nano, tzdata, and unzip


## Notes:

By default, the image runs jupyter lab on the path `/home/jovyan/work/notebooks`

FROM jupyter/datascience-notebook:2ce7c06a61a1

WORKDIR /home/jovyan/work

# Add Kernels
RUN npm install -g ijavascript && ijsinstall && \
  npm install -g typescript itypescript && its --install=local && \
  pip install bash_kernel && python -m bash_kernel.install

# NPM packages
RUN npm init -y && tsc --init && \
  npm install dstools && \
  npm install rxjs && \
  npm install lodash && \
  npm install circos && \
  npm install plotly-notebook-js

# R packages
RUN echo 'local({r <- getOption("repos"); r["CRAN"] <- "http://cloud.r-project.org"; options(repos=r)})' >> ~/.Rprofile && \
  Rscript -e 'install.packages(c("circlize", "plotly"))'

# Python packages
RUN pip install \
  plotly==4.1.0 \
  plotly-geo==1.0.0 \
  cufflinks==0.16 \
  pymongo==3.9.0 \
  python-dotenv==0.10.3 \
  jupyter-contrib-nbextensions==0.5.1 \
  jupytext==1.2.1 \
  rise==5.5.1 \
  voila==0.1.9 \
  jupyterlab-dash==0.1.0a3 \
  papermill==1.0.1 \
  pylantern==0.1.4 \
  tqdm==4.33.0 \
  auto_tqdm==1.0.3 \
  sidecar==0.3.0 \
  qgrid==1.1.1 \
  pandas-profiling==2.3.0 \
  pytest==5.0.1 \
  ipytest==0.7.1 \
  cerberus==1.3.1 \
  pyforest==0.1.1 && \
  fix-permissions $CONDA_DIR && \
  fix-permissions /home/$NB_USER

# Jupyter extensions
RUN jupyter contrib nbextension install --user
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager
RUN jupyter labextension install jupyterlab-jupytext
RUN jupyter labextension install plotlywidget
RUN jupyter labextension install jupyterlab-plotly
RUN jupyter labextension install jupyterlab-chart-editor
RUN jupyter labextension install jupyterlab_voyager
RUN jupyter labextension install jupyterlab-dash@0.1.0-alpha.3
RUN jupyter labextension install qgrid
RUN jupyter labextension install @jpmorganchase/perspective-jupyterlab
RUN jupyter labextension install ipysheet
RUN jupyter labextension install lineup_widget
RUN jupyter labextension install @jupyter-widgets/jupyterlab-sidecar
RUN jupyter labextension install @jupyterlab/toc
RUN jupyter nbextension install --py jupytext --user
RUN jupyter nbextension enable --py jupytext --user
RUN jupyter nbextension enable --py widgetsnbextension --user
RUN jupyter nbextension enable hinterland/hinterland --user
RUN jupyter nbextension enable hide_input_all/main --user
RUN jupyter nbextension enable splitcell/splitcell --user
RUN jupyter nbextension enable varInspector/main --user
RUN jupyter nbextension enable codefolding/main --user
RUN jupyter nbextension enable scratchpad/main --user
RUN jupyter nbextension enable collapsible_headings/main --user
RUN jupyter nbextension enable execute_time/ExecuteTime --user
RUN jupyter nbextension enable toc2/main --user
RUN jupyter nbextension enable notify/notify --user
RUN jupyter nbextension enable --py --sys-prefix qgrid --user
RUN jupyter serverextension enable voila --sys-prefix
RUN jupyter labextension install @jupyterlab/git
RUN pip install --upgrade jupyterlab-git
RUN jupyter serverextension enable --py jupyterlab_git
# RUN jupyter labextension install pylantern
# RUN jupyter serverextension enable --py lantern

RUN jupyter lab clean && jupyter lab build

# Configure Jupytext
ARG nb_config=../.jupyter/jupyter_notebook_config.py
RUN echo 'c.NotebookApp.contents_manager_class = "jupytext.TextFileContentsManager"' >> $nb_config && \
  echo 'c.ContentsManager.default_jupytext_formats = "ipynb,auto"' >> $nb_config && \
  echo 'c.ContentsManager.preferred_jupytext_formats_save = "auto:percent"' >> $nb_config

# Install ssh
USER root
RUN apt update && apt install openssh-server -y

# Skip Host verification for git
RUN mkdir /etc/ssh -p && \
  echo "Host *" >> /etc/ssh/ssh_config && \
  echo "   StrictHostKeyChecking no" >> /etc/ssh/ssh_config && \
  echo "   UserKnownHostsFile=/dev/null" >> /etc/ssh/ssh_config

# install plotly-orca and dependencies
RUN conda install -y -c plotly plotly-orca psutil
RUN apt-get install -y --no-install-recommends \
  wget \
  xvfb \
  libgtk2.0-0 \
  libxtst6 \
  libxss1 \
  libgconf-2-4 \
  libnss3 \
  libasound2 && \
  mkdir -p /home/orca && \
  cd /home/orca && \
  wget https://github.com/plotly/orca/releases/download/v1.2.1/orca-1.2.1-x86_64.AppImage && \
  chmod +x orca-1.2.1-x86_64.AppImage && \
  ./orca-1.2.1-x86_64.AppImage --appimage-extract && \
  printf '#!/bin/bash \nxvfb-run --auto-servernum --server-args "-screen 0 640x480x24" /home/orca/squashfs-root/app/orca "$@"' > /usr/bin/orca && \
  chmod +x /usr/bin/orca

USER jovyan

RUN mkdir /home/jovyan/work/notebooks

WORKDIR /home/jovyan/work/notebooks

CMD ["jupyter", "lab"]
